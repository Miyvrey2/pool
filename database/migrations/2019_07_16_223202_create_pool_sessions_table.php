<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoolSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pool_sessions', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('player_1')->unsigned();
            $table->foreign('player_1')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('player_2')->unsigned();
            $table->foreign('player_2')->references('id')->on('users')->onDelete('cascade');
            $table->integer('player_1_score')->nullable();
            $table->integer('player_2_score')->nullable();
            $table->string('status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pool_sessions');
    }
}
