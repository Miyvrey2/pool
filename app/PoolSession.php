<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PoolSession extends Model
{
    // Use soft deletes
    use SoftDeletes;

    // Table name
    protected $table = 'pool_sessions';

    // Rows we may fill
    protected $fillable = ['player_1', 'player_2', 'status', 'player_1_score', 'player_2_score'];

    public function scopeWithPlayerId($query, $user_id)
    {
        return $query->where([
            ['player_1', '=', $user_id],
        ])->orWhere([
            ['player_2', '=', $user_id],
        ]);
    }

    public function scopeWithPlayerIds($query, $player1_id, $player2_id)
    {
        return $query->withPlayerId($player1_id)
                   ->withPlayerId($player2_id);
    }

    public function scopeActiveSessionsWithPlayerId($query, $player1_id)
    {
        return $query->withPlayerId($player1_id)
                     ->whereDate('created_at', Carbon::today())
                     ->whereIn('status', ['suggestion', 'match']);
    }

    public function scopeActiveSessionsWithPlayerIds($query, $player1_id, $player2_id)
    {
        return $query->withPlayerIds($player1_id, $player2_id)
                     ->whereDate('created_at', Carbon::today())
                     ->whereIn('status', ['suggestion', 'match']);
    }

    public function scopeThisWeek($query)
    {
        $now = Carbon::now();

        return $query->where([
            ['created_at', '>=', $now->startOfWeek()->format('Y-m-d H:i')],
            ['created_at', '<=', $now->endOfWeek()->format('Y-m-d H:i')],
        ]);
    }

    public function player1()
    {
        return $this->belongsTo(User::class, 'player_1', 'id');
    }

    public function player2()
    {
        return $this->belongsTo(User::class, 'player_2', 'id');
    }
}
