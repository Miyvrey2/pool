<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Notification;
use App\PoolSession;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{

    /**
     * Register a new device to send notifications to.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return string
     */
    public function registerToken(Request $request)
    {
        $data = $request->except('_token');
        $data['user_id'] = Auth::user()->id;

        Notification::updateOrCreate($data);

        return "true";
    }

    /**
     * Register a new device to send notifications to.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return string
     */
    public function unregisterToken(Request $request)
    {
        $notificationDevices = Auth::user()->notifications;

        foreach($notificationDevices as $device) {
            $device->delete();
        }
    }

    public function sendNotificationToUser($user)
    {
        $notificationDevices = $user->notifications;

        foreach($notificationDevices as $device) {
            $this->sendNotification($device->token);
        }
    }

    public function sendNotification($to)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $token = 'AAAAjo0mUsU:APA91bHzg9JnkYOx1R0cFsBodJAzqD1WCYJI6taiVJKTBpYkh6YGobNFiUEgwbB7QcpRt8guOvQb9bI-1OjmVGmeYUbGOQMeE2wL8Wdo0efarRNg_Cexe-CtcMvFQKrwv8_zZ7NWLq6V';


        $notification = [
            'body' => Auth::user()->name . ' challenged you to battle!',
            'sound' => true,
        ];

        $extraNotificationData = ["message" => $notification,"moredata" =>'dd'];

        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to'        => $to, //single token
            'notification' => $notification,
            'data' => $extraNotificationData
        ];

        $headers = [
            'Authorization: key=' . $token,
            'Content-Type: application/json'
        ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);


        return $result;
    }
}
