<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Notification;
use App\Notifications\userChallenged;
use App\PoolSession;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PoolController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function choosePlayer2()
    {
        $notification = new NotificationController();

        // Get active sessions
        $pool_sessions = PoolSession::activeSessionsWithPlayerId(Auth::user()->id)->get();

        // If there any pending sessions, return
        if(isset($pool_sessions[0]) && $pool_sessions[0] != null) {

            // Return the user_object that is not the authenticated user
            if(Auth::user()->id != $pool_sessions[0]->player_1) {
                $player2 = User::find($pool_sessions[0]->player_1);
            } else {
                $player2 = User::find($pool_sessions[0]->player_2);
            }

            if($pool_sessions[0]->status = 'suggestion') {
                $message = $this->alert_message('You still got a match to play with ' . $player2->name . '.');
            }

            if($pool_sessions[0]->status = 'match') {
                $message = $this->alert_message('You were stil in a fiercy match with ' . $player2->name . '!');
            }

            // Send web notification
            $notification->sendNotificationToUser($player2);

            // Send Slack notification
            $player2->notify(new userChallenged($player2));

            return compact('player2', 'message');
        }


        // If there player that didn't play today, select them first
        $players_played_today = $this->players_played_today();
        array_push($players_played_today, Auth::user()->id);
        $player2 = User::whereNotIn('id', $players_played_today)->online()->first();

        if($player2 != null){
            $this->set_player_2_suggestion($player2->id);
            $message = $this->alert_message($player2->name . ' didn\'t play today yet (untill now!)');

            // Send web notification
            $notification->sendNotificationToUser($player2);

            // Send Slack notification
            $player2->notify(new userChallenged($player2));

            return compact('player2', 'message');
        }


        // Get a random user
        $player2 = User::where([['online', '=',1],['id', '!=', Auth::user()->id]])->get();
        if(!$player2->isEmpty()) {
            $player2 = $player2->random();
            $this->set_player_2_suggestion($player2->id);
            $message = $this->alert_message($player2->name . ' got chosen by the magnificent bits and bytes of the atomic clock!');
        } else {
            $message = $this->alert_message('I guess you are all on your own today. That, or some magic appeared right in front of the DB connection.');
        }


//        $notification->sendNotificationToUser(Auth::user());
        return compact('player2', 'message');
    }

    public function getMatch()
    {
        // Get active sessions
        $pool_sessions = PoolSession::activeSessionsWithPlayerId(Auth::user()->id)->get();

        return view('frontend.pool.getmatch', compact('pool_sessions'));
    }

    public function match(Request $request)
    {
        // Get active sessions
        $pool_session = PoolSession::activeSessionsWithPlayerIds($request['player1'], $request['player2'])->get();

        // If there is no collection, it was probably deleted
        if( $pool_session->isEmpty() ) {
            return "Whoops, something went wrong. Did you delete the suggested match?";
        }

        // If, for some odd reason, 2 or more sessions exist, return
        // TODO: Delete the multiple sessions
        if( $pool_session->count() > 1 ) {
            return "Whoops, something went wrong. You have multiple suggested matches like this. How did you even get this?";
        }

        // Save the current session as match
        $data['status'] = "match";
        $pool_session[0]->update($data);

        // Get the players
        $player1 = User::find($request['player1']);
        $player2 = User::find($request['player2']);

        return view('frontend.pool.match', compact('player1', 'player2'));
    }

    public function set(Request $request)
    {
        $data = $request->all();

        $pool_session = PoolSession::withPlayerIds($data['player1'], $data['player2'])
                                   ->whereDate('created_at', Carbon::today())
                                   ->whereStatus('match')
                                   ->get();

        // If there is no collection, it was probably deleted
        if( $pool_session->isEmpty() ) {
            return "Whoops, something went wrong. Did you delete the suggested match?";
        }

        if( $pool_session->count() > 1 ) {
            return "Whoops, something went wrong. You have multiple suggested matches like this. How did you even get this?";
        }

        if( $this->check_score($data) != false ) {

            // Save the current session as set
            $data['status'] = "set";
            $pool_session[0]->update($data);
        }


        $player1 = User::find($request['player1']);
        $player2 = User::find($request['player2']);

        return view('frontend.pool.set', compact('data', 'player1', 'player2'));
    }

    private function players_played_today()
    {
        // Get today's sessions
        $pool_sessions = PoolSession::whereDate('created_at', Carbon::today())->get();

        // Setup an array where ID's can be stored
        $players_that_played = [];

        // Loop trough today's sessions and save the players
        foreach($pool_sessions as $pool_session) {
            array_push($players_that_played, $pool_session['player_1']);
            array_push($players_that_played, $pool_session['player_2']);
        }

        return array_unique($players_that_played);
    }

    private function set_player_2_suggestion($user_id)
    {
        $data = [
            'player_1' => Auth::user()->id,
            'player_2' => $user_id,
            'status'   => 'suggestion'
        ];

        PoolSession::create($data);
    }

    private function check_score($data)
    {
        if (( $data['player_1_score'] == 0 && $data['player_2_score'] == 2 ) ||
            ( $data['player_1_score'] == 1 && $data['player_2_score'] == 1 ) ||
            ( $data['player_1_score'] == 2 && $data['player_2_score'] == 0 )) {
            return $data;
        }

        return false;
    }
}
