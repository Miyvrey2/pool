<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\PoolSession;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ScoreController extends Controller
{
    //
    public function index()
    {
        // Get pool sessions from this week
        $pool_sessions = PoolSession::thisWeek()->get();

        $scoreList = [];

        foreach($pool_sessions as $pool_session) {

            if(!array_key_exists($pool_session->player_1, $scoreList)) {
                $scoreList[$pool_session->player_1] = 0;
            }

            if(!array_key_exists($pool_session->player_2, $scoreList)) {
                $scoreList[$pool_session->player_2] = 0;
            }

            $scoreList[$pool_session->player_1] += $pool_session->player_1_score;
            $scoreList[$pool_session->player_2] += $pool_session->player_2_score;
        }

        arsort($scoreList);

        $users = User::all();
        foreach($users as $user) {
            if(isset($scoreList[$user->id])) {
                $user['scores'] = $scoreList[$user->id];
            } else {
                $user['scores'] = '-';
            }
        }

        $users = $users->sortByDesc('scores');

        return view('frontend.score.index', compact('users', 'pool_sessions'));

    }
}
