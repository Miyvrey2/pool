<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\PoolSession;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PlayerController extends Controller
{
    //
    public function index()
    {
        $players = User::all();

        return view('frontend.player.index', compact('players'));

    }

    public function massUpdate(Request $request)
    {
        $players = $request->except('_token');

        foreach($players as $player => $online) {
            User::where('id', $player)->update(['online' => $online]);
        }


        $message = $this->alert_message('Updated players!');

        return redirect()->back()->with($message);

    }
}
