<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\Frontend\NotificationController;

Route::get('/', 'Frontend\HomeController@index')->name('home');

Auth::routes();

Route::post('/pool/choosePlayer2', 'Frontend\PoolController@choosePlayer2');
Route::get('/pool/match', 'Frontend\PoolController@getMatch');
Route::post('/pool/match', 'Frontend\PoolController@match');
Route::post('/pool/set', 'Frontend\PoolController@set');

Route::resource('/scores', 'Frontend\ScoreController');

Route::get('players', 'Frontend\PlayerController@index');
Route::post('players', 'Frontend\PlayerController@massUpdate');

Route::post('registerToken', 'Frontend\NotificationController@registerToken');