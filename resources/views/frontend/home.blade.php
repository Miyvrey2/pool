@extends('frontend.layouts.app')

@section('content')
    <style>

        form {
            position: relative;
            margin: 40px auto 0;
            text-align: center;
        }

        .form-group select,
        .form-group input {
            text-align: center;
        }

        .form-group select option {
            text-align: center;
        }

    </style>

    <div class="container home">
        <div class="row">
            <div class="offset-md-3 col-md-6">

                <h1>Match</h1>

                @if(Auth::user())
                <form method="post" id="selectPlayers" action="{{ url('/pool/match') }}">

                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="player1">Player 1</label><br>
                        <select id="player1" name="player1">
                            <option value="{{ Auth::user()->id }}" selected>{{ Auth::user()->name }}</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="player2_name">Player 2</label><br>
                        <input type="text" id="player2_name" name="player2_name" value="-- No value --" disabled/>
                        <input type="hidden" id="player2" name="player2" value="" />
                    </div>

                    <div class="form-group">
                        <button id="choosePlayer2" class="btn btn-primary button-full">Click to find Player 2</button>
                        <button id="startPlaying" class="btn btn-primary-outline button-full" style="display: none;" disabled>Start playing!</button>
                        <a href="#">Whoops, not playing after all?</a>
                    </div>
                </form>

                <script src="https://code.jquery.com/jquery-1.10.2.js"></script>

                <script type="text/javascript">

                    var submits = 0;

                    $( "#choosePlayer2" ).click(function( event ) {

                        // Stop form from submitting normally
                        event.preventDefault();

                        // Stop hitting that button
                        $('#player2_name').removeAttr('value').attr('value', 'searching players');
                        $('#choosePlayer2').prop("disabled", true);

                        // Get some values from elements on the page:
                        var $form = $( this );
                        var _token = $( "input[name='_token']" ).val();
                        var player1 = $( "#player1" ).val();
                        var player2 = $( "#player2" ).val();

                        submits++;

                        if (submits > 1) {
                            alert('You already did a request so go and play nicely instead of breaking in 😊');
                        } else {
                            $.post( "{{ url('/pool/choosePlayer2') }}", { _token: _token, player1: player1, player2: player2 })
                                .done(function( data ) {
                                    $('#player2_name').removeAttr('value').attr('value', data.player2.name);
                                    $('#player2').removeAttr('value').attr('value', data.player2.id);
                                    $('#choosePlayer2').removeClass('btn-primary').hide();
                                    $('#startPlaying').prop("disabled", false).addClass('btn-primary').removeClass('btn-primary-outline').show();
                                    $('.message .alert').addClass('show alert-success').find('strong').html(data.message.content);
                                });
                        }
                    });
                </script>
                @endif

            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="js/firebase-app.js"></script>
    <script src="js/firebase-messaging.js"></script>
    <script src="js/script.js"></script>

    @if(Auth::user())
    @else
        <script>
            setTokenSentToServer(0);
        </script>
    @endif

@endsection
