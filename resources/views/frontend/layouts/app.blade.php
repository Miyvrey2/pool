<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="manifest" href="manifest.json">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="favicon.ico">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Stylesb -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    <style>
        html, body {
            background-color: #fff;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .message {
            position: absolute;
            width: calc(100vw - 30px);
            left: 15px;
            bottom: 10px;
        }
        .message .alert {
            display: none;
        }
        .message .alert.show {
            display: block;
        }
        .message .alert-success {
            border: 1px solid rgba(29, 100, 59, 0.4);
        }
        .message .alert .close {
            margin-top: 0;
            margin-right: 0;
        }

    </style>
</head>
<body>
    <header>
        <a id="logo" href="{{ url('') }}">Pool App<span>V3.0</span></a>
        <nav>
            @auth
                <a href="{{ url('/') }}">Home</a>

                <a href="{{ url('/players') }}">Players</a>

                <a href="{{ url('/scores') }}">Scores</a>

                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            @else
                <a href="{{ route('login') }}">Login</a>

                @if (Route::has('register'))
                    <a href="{{ route('register') }}">Register</a>
                @endif
            @endauth
        </nav>
    </header>
    <div id="app">
        <div class="message">
            <div class="alert">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>Success!</strong> {{ Session::get('message', '') }}
            </div>
        </div>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
