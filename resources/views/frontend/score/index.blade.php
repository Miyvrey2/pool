@extends('frontend.layouts.app')

@section('content')

    <style>
        table {
            width: 100%;
        }

        th {
            background-color: #4CAF50;
            color: white;
        }

        th,
        td {
            padding: 5px 10px;
        }

        th:nth-of-type(1),
        td:nth-of-type(1) {
            text-align: center;
        }

        th:nth-of-type(3),
        td:nth-of-type(3) {
            text-align: center;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        #scoreList th:nth-of-type(1),
        #scoreList td:nth-of-type(1) {
            width: 50px;
        }

        #matchList th,
        #matchList td {
            text-align: center;
        }

        h2 {
            margin-bottom: 15px;
        }

    </style>

    <div class="container">
        <div class="row">
            <div class="offset-md-3 col-md-6">

                <h1>Pool App<span>V3.0</span></h1>

                @if(Auth::user())
                    <h2>Highscore</h2>
                    <table id="scoreList">
                        <thead>
                            <tr>
                                <th>Ranking</th>
                                <th>Name</th>
                                <th>Score</th>
                            </tr>
                        </thead>
                        @php($i = 1)
                        @foreach($users as $user)
                            <tr>
                                <td>#{{ $i }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->scores }}</td>
                            </tr>
                            @php($i++)
                        @endforeach
                    </table>

                    <h2>Matches this week</h2>
                    <table id="matchList">
                        <thead>
                        <tr>
                            <th>Player 1</th>
                            <th>Score</th>
                            <th>Player 2</th>
                        </tr>
                        </thead>
                        @php($i = 1)
                        @foreach($pool_sessions as $pool_session)
                            <tr>
                                <td>{{ $pool_session->player1->name }}</td>
                                <td>{{ $pool_session->player_1_score }} - {{ $pool_session->player_2_score }}</td>
                                <td>{{ $pool_session->player2->name }}</td>
                            </tr>
                            @php($i++)
                        @endforeach
                    </table>

                @endif

            </div>
        </div>
    </div>
@endsection
