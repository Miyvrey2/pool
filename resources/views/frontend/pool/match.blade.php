@extends('frontend.layouts.app')

@section('content')
    <style>
        h1 {
            width: 100%;
            margin: 30px 0 0 20px;
            text-align: center;
        }

        h1 span {
            display: inline-block;
            padding-top: 5px;
            font-size: 15px;
            vertical-align: top;
        }

        form {
            position: relative;
            margin: 40px auto 0;
            text-align: center;
        }
        .form-group {
        }

        .form-group select,
        .form-group input {
            width: 100%;
            box-sizing: border-box;

            height: 40px;
            display: block;
            padding: 10px;
            border: 1px solid #CECECE;
            border-radius: 2px;
            color: #444;
            line-height: 1;
            text-align: center;

        }

        .form-group select option {
            text-align: center;
        }

        .form-group input[type="submit"] {
            color: white;
        }

        button {
            margin: 10px;
        }

    </style>

    <div class="container">
        <div class="row">
            <div class="offset-md-3 col-md-6">

                <h1>Pool App<span>V3.0</span></h1>

                @if(Auth::user())
                <form method="post" id="selectPlayers" action="{{ url('/pool/set') }}">

                    {{ csrf_field() }}

                    <div class="row">
                        <div class="form-group col-md-5">
                            <input type="hidden" id="player1" name="player1" value="{{ $player1->id }}" />
                            <label for="player_1_score">{{ $player1->name }}</label><br>
                            <select id="player_1_score" name="player_1_score">
                                <option value="null" selected>-- No value --</option>
                                <option value="0">Loss</option>
                                <option value="1">Draw</option>
                                <option value="2">Win</option>
                            </select>
                        </div>

                        <div class="col-md-2">
                            <br><label>vs.</label>
                        </div>

                        <div class="form-group col-md-5">
                            <input type="hidden" id="player2" name="player2" value="{{ $player2->id }}" />
                            <label for="player_2_score">{{ $player2->name }}</label><br>
                            <input type="hidden" id="player_2_score" name="player_2_score" value="" />
                            <input type="text" id="player_2_title" name="player_2_title" value="-- No value --" disabled />
                        </div>
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-primary button-full" value="Match settled" /><br>
                        <a href="#">Whoops, got disrupted?</a>
                    </div>
                </form>

                <script src="https://code.jquery.com/jquery-1.10.2.js"></script>

                <script type="text/javascript">

                    $( "#player_1_score" ).change(function( event ) {

                        // Stop form from submitting normally
                        event.preventDefault();

                        var player_1_score = $( this ).val();
console.log(player_1_score);
                        if( player_1_score == 0) {
                            $("#player_2_score").removeAttr('value').attr('value', 2);
                            $("#player_2_title").removeAttr('value').attr('value', "Win");
                        } else if( player_1_score == 1) {
                            $("#player_2_score").removeAttr('value').attr('value', 1);
                            $("#player_2_title").removeAttr('value').attr('value', "Draw");
                        } else if( player_1_score == 2) {
                            $("#player_2_score").removeAttr('value').attr('value', 0);
                            $("#player_2_title").removeAttr('value').attr('value', "Loss");
                        } else {
                            $("#player_2_score").removeAttr('value').attr('value', "null");
                            $("#player_2_title").removeAttr('value').attr('value', "-- No value --");
                        }


                    });
                </script>
                @endif

            </div>
        </div>
    </div>
@endsection
