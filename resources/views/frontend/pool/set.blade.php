@extends('frontend.layouts.app')

@section('content')
    <style>
        h1 {
            width: 100%;
            margin: 30px 0 0 20px;
            text-align: center;
        }

        h1 span {
            display: inline-block;
            padding-top: 5px;
            font-size: 15px;
            vertical-align: top;
        }

        form {
            position: relative;
            margin: 40px auto 0;
            text-align: center;
        }
        .form-group {
        }

        .form-group select,
        .form-group input {
            width: 100%;
            box-sizing: border-box;

            height: 40px;
            display: block;
            padding: 10px;
            border: 1px solid #CECECE;
            border-radius: 2px;
            color: #444;
            line-height: 1;
            text-align: center;

        }

        .form-group select option {
            text-align: center;
        }

        .form-group input[type="submit"] {
            color: white;
        }

        button {
            margin: 10px;
        }

    </style>

    <div class="container">
        <div class="row">
            <div class="offset-md-3 col-md-6">

                <h1>Pool App<span>V3.0</span></h1>

                @if(Auth::user())

                    @if($data['player_1_score'] == 2)
                        <h2>Congrats {{ $player1->name }}</h2>
                    @endif

                    @if($data['player_2_score'] == 2)
                        <h2>Congrats {{ $player2->name }}</h2>
                    @endif

                    @if($data['player_1_score'] == 1)
                        <h2>Congrats on you both! </h2>
                    @endif

                @endif

            </div>
        </div>
    </div>
@endsection
