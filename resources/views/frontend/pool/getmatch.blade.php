@extends('frontend.layouts.app')

@section('content')
    <style>
        h1 {
            width: 100%;
            margin: 30px 0 0 20px;
            text-align: center;
        }

        h1 span {
            display: inline-block;
            padding-top: 5px;
            font-size: 15px;
            vertical-align: top;
        }

        form {
            position: relative;
            margin: 40px auto 0;
            text-align: center;
        }
        .form-group {
        }

        .form-group select,
        .form-group input {
            width: 100%;
            box-sizing: border-box;

            height: 40px;
            display: block;
            padding: 10px;
            border: 1px solid #CECECE;
            border-radius: 2px;
            color: #444;
            line-height: 1;
            text-align: center;

        }

        .form-group select option {
            text-align: center;
        }

        .form-group input[type="submit"] {
            color: white;
        }

        button {
            margin: 10px;
        }

    </style>

    <div class="container">
        <div class="row">
            <div class="offset-md-3 col-md-6">

                <h1>Pool App<span>V3.0</span></h1>

                @if(Auth::user())

                    @foreach($pool_sessions as $session)
                    <form method="post" action="{{ url('/pool/match') }}">

                        {{ csrf_field() }}

                        <input type="hidden" id="player1" name="player1" value="{{ Auth::user()->id }}" />
                        <input type="hidden" id="player2" name="player2" value="{{ $session->player_2 }}" />

                        <input type="submit" value="{{ $session->status }} against {{ $session->player_2 }}" />
                    </form>
                    @endforeach

                @endif

            </div>
        </div>
    </div>
@endsection
