@extends('frontend.layouts.app')

@section('content')

    <style>
        table {
            width: 100%;
        }

        th {
            background-color: #4CAF50;
            color: white;
        }

        th,
        td {
            padding: 5px 10px;
        }

        th:nth-of-type(1),
        td:nth-of-type(1) {
            text-align: center;
        }

        th:nth-of-type(3),
        td:nth-of-type(3) {
            text-align: center;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        #scoreList th:nth-of-type(1),
        #scoreList td:nth-of-type(1) {
            width: 50px;
        }

        #matchList th,
        #matchList td {
            text-align: center;
        }

        h2 {
            margin-bottom: 15px;
        }

        input[type='checkbox'] {
            position: relative;
            width: 30px;
            height: 15px;
            background-color: #DDD;
            border: 0;
            border-radius: 15px;
            outline: 0;
            vertical-align: middle;
            -webkit-appearance: unset;
        }

        input[type='checkbox']:after {
            content: '';
            position: absolute;
            width: 13px;
            height: 13px;
            top: 1px;
            left: 1px;
            background-color: #555;
            border-radius: 6.5px;
            -webkit-transition: all 0.2s;
            -moz-transition: all 0.2s;
            -ms-transition: all 0.2s;
            -o-transition: all 0.2s;
            transition: all 0.2s;
        }

        input[type='checkbox']:checked:after {
            margin-left: 15px;
            background-color: #4CAF50;
        }

        .playerList {
            padding: 0;
            list-style: none;
        }

        .playerList li:hover {
            background-color: #F8F8F8;
        }

        .playerListName {
            width: calc(50% - 15px);
            display: inline-block;
            padding-left: 15px;
        }
        .playerListOnline {
            width: 49%;
            display: inline-block;
            text-align: right;
        }

    </style>

    <div class="container">
        <div class="row">
            <div class="offset-md-3 col-md-6">

                <h1>Pool App<span>V3.0</span></h1>

                <form method="post" action="{{ url('/players') }}">

                    {{ csrf_field() }}

                    <ul class="playerList">
                        @foreach($players as $player)
                        <input type="hidden" name="{{ $player->id }}" value="0" />
                        <li><span class="playerListName">{{ $player->name }}</span> <span class="playerListOnline"><input type="checkbox" name="{{ $player->id }}" @if($player->online)checked @endif value="1"/></span></li>
                        @endforeach
                    </ul>

                    <input type="submit" value="Update players" />

                </form>

            </div>
        </div>
    </div>
@endsection
